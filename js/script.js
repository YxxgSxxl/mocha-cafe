// Suppression du message d'alerte de la topbar
let topbar_exit = document.querySelector('.topbar-exit'); // L'icone de croix
let topbar_alert = document.querySelector('.topbar-alert'); // Le menu d'alerte

topbar_exit.addEventListener('click', () => {
    topbar_alert.classList.add('hide'); // Ajout de la classe hide (display: none)
})

// Ouverture et fermeture du menu (mobile)
let menu_burgericon = document.getElementById('menu-toggle'); // L'icone burger
let menu_nav = document.querySelector('.menu-nav'); // Le menu de navigation

menu_burgericon.addEventListener('click', () => {
    menu_burgericon.classList.toggle('nav-open'); // Transformation de l'icône en croix
    menu_nav.classList.toggle('show'); // Affichage du menu
})

// Sous lignage du titre h1 de la page au scroll
const title = document.querySelector('span.title-colored');
const scrollValue = 350; // La valeure du scroll pour activer le sous-lignage

document.addEventListener('scroll', function() {
    if (window.scrollY >= scrollValue) {
        title.classList.add('title-colored--active');
    } else {
        title.classList.remove('title-colored--active');
    }
 })