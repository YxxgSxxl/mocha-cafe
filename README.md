# Mocha Café  
<a href="https://mocha-cafe.vercel.app/">Le lien du projet déployé</a>

## Explications
Pour ce projet, j'ai du mettre en place une maquette d'un site internet de vente de café.  

-> Ce site a été developpé en mobile first.  
-> La librairie AOS (Animate On Scroll) a été utilisée pour le texte "Buvez un café d'Exception".

Dans un premier temps, j'ai developpé la partie HTML.  

Dans un second temps, j'ai developpé le style CSS.  

Puis, j'ai dynamisé la page avec du JS.  

Puis, j'ai ajouté du responsive au site pour l'adapter au format desktop.  

Et enfin, le projet sera déployé avec <a href="https://vercel.com/">Vercel</a>.

## Explications additionnelles

Les commandes GIT que j'ai utilisé pour démarrer mon projet:  

* git init  
* git clone https://gitlab.com/YxxgSxxl/mocha-cafe  
* git remote add origin https://gitlab.com/YxxgSxxl/mocha-cafe.git  
* git remote -v  
* git branch -M main  
* git add .  
* git status  
* git commit -m "message"  
* git push -uf origin main  

Les images ont étés optimisées pour le web à l'aide de <a href="https://tinypng.com/">TinyPNG</a>.